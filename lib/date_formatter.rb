module DateFormatter

  FORMATTED_METHOD = /^formatted_(.+)$/

  # ensure that respond_to? returns true if passed a valid formatted_method_name
  def respond_to? method_name, some_random_argument = nil
    return true if valid_formatted_method_name? method_name
    super
  end

  # call strftime on the given date, with this class's strftime_date_string
  def format_date value
    value.respond_to?(:strftime) ? value.strftime(strftime_date_string) : value
  end

  # generic date formatter
  # called like formatted_method_name
  # if your class responds to method_name, call format_date with it
  def method_missing method_name, *args
    if name = valid_formatted_method_name?(method_name)
      value = send name
      return format_date value
    else
      super
    end
  end

  # set a default value for strftime_date_string
  # override this method in your class to customize the date format
  def strftime_date_string
    '%m/%d/%Y'
  end

private

  def valid_formatted_method_name? method
    return $1 if method.to_s =~ DateFormatter::FORMATTED_METHOD and respond_to?($1)
  end

end

%w[ rubygems spec active_support ].each { |f| require f }
require File.join(File.dirname(__FILE__), '..', 'lib', 'date_formatter')

class DateFormatterClass

  include DateFormatter
  attr_accessor :given_date, :other_date

  def initialize options = {}
    options.each { |k,v| instance_variable_set "@#{ k }", v }
  end

end

describe DateFormatter do

  before :all do
    @given_date = 3.days.ago
    @other_date = 2.weeks.ago

    @date_formatter = DateFormatterClass.new :given_date => @given_date, :other_date => @other_date
  end

  it 'returns a formatted date' do
    @date_formatter.formatted_given_date.should == @given_date.strftime('%m/%d/%Y')
    @date_formatter.formatted_other_date.should == @other_date.strftime('%m/%d/%Y')
  end

  describe '#strftime_date_string' do

    it 'defines a default format string' do
      @date_formatter.strftime_date_string.should == '%m/%d/%Y'
    end

  end

  describe '#responds_to?' do

    it 'returns true if the class responds to the specified field' do
      @date_formatter.should     respond_to(:formatted_given_date)
      @date_formatter.should     respond_to(:formatted_other_date)
    end

    it 'returns false if the class does not respond to the specified field' do
      @date_formatter.should_not respond_to(:formatted_not_a_date)
    end

  end

  describe '#format_date' do

    it 'formats the given date' do
      @date_formatter.format_date(Date.today).should == Date.today.strftime('%m/%d/%Y')
    end

    it 'returns the given date if it does not respond to strftime' do
      @date_formatter.format_date('the date').should == 'the date'
    end

  end

end

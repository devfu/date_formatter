Date Formatter
==============

Simple date formatter using method_missing.
Format dates with a common format. Useful for making views and specs more readable.

Usage:

    class Foo
      include DateFormatter
      attr_accessor :some_date
    end
    
    Foo.new.formatted_some_date # => returns a pretty date

Override strftime_date_string to customize the format used.

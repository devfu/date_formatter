Gem::Specification.new do |s|
  s.name        = 'date_formatter'
  s.version     = '0.0.2'
  s.summary     = 'Simple date formatter using method_missing'
  s.description = 'Format dates with a common format. Useful for making views and specs more readable.'
  s.files       = Dir['lib/**/*.rb']
  s.author      = 'Dev Fu!'
  s.email       = 'info@devfu.com'
  s.homepage    = 'http://github.com/devfu/date_formatter'
end
